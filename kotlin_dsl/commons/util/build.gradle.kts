plugins {
    id("java-library-convention")
}

dependencies {
    api(platform(project(":platform")))
}

configure<info.solidsoft.gradle.pitest.PitestPluginExtension> {
    targetClasses.set(setOf("com.example.util.*"))
}
