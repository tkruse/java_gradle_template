# project build-conventions

The convention classes here can be added to projects like other plugins, i.e.:

    plugins {
        id("name-of-convention")
    }

Changes here do not cause general rebuild of whole project, only affected submodules would rebuild.

Use this to share simple declarational build logic between modules.
