plugins {
    `java-library`
}

// Common dependencies for any java-library module in this project
dependencies {
    components.all<AlignmentRules>()
    implementation(Libraries.commons_lang3)
    implementation(Libraries.commons_coll4)
    implementation(Libraries.guava)
    // required for guava :-(
    implementation("com.google.errorprone:error_prone_annotations") {
        version {
            prefer(ToolVersions.errorProne)
        }
    }

    // provides javax.annotationsNonnull and CheckForNull, no runtime dependency
    compileOnly(Libraries.jsr305)
    compileOnly(Libraries.spotbugsAnnotations)
    testCompileOnly(Libraries.jsr305)
    testCompileOnly(Libraries.spotbugsAnnotations)

    implementation(Libraries.slf4j_api)
    implementation(enforcedPlatform("org.slf4j:slf4j-virtual-platform:${Versions.slf4j}"))

    runtimeOnly(Libraries.jcl_over_slf4j)
    runtimeOnly(Libraries.log4j_over_slf4j)
    runtimeOnly(Libraries.logback)

    implementation(enforcedPlatform("com.fasterxml.jackson:jackson-bom:${Versions.jackson}"))

    // test dependencies
    testImplementation(Libraries.junit) {
        version {
            strictly(Versions.junit)
        }
    }
    testImplementation(Libraries.hamcrest)
    testImplementation(Libraries.assertj_core)
    testImplementation(Libraries.mockito2) {

    }
    testImplementation(Libraries.powermock) {
        // use mockito version2
        exclude(group = "net.bytebuddy")
        exclude(group = "org.objenesis")
    }
    testImplementation(Libraries.powermock_mockito2) {
        exclude(group = "org.mockito")
        // use mockito version2
        exclude(group = "net.bytebuddy")
        exclude(group = "org.objenesis")
    }
    testImplementation(Libraries.awaitility)
}

// restrict transitive dependencies for java projects.
configurations {

    implementation {
        // jackson2 has new group com.fasterxml.jackson. Old one sometimes needed by test libraries
        exclude(group = "org.codehaus.jackson")
        // hibernate-validator moved to group org.hibernate.validator
        exclude(group = "org.hibernate", module = "hibernate-validator")
    }


    all {
        // using mockito2
        exclude(module = "mockito-all")
        // no log4j, we use logback
        exclude(module = "log4j")
        exclude(module = "slf4j-log4j12")

        // using jcl-over-slf4j
        exclude(module = "commons-logging")
        // using hamcrest-all instead
        exclude(module = "hamcrest-core")
        exclude(module = "hamcrest-library")
    }
}

setupResolutionStrategy(listOf(
        configurations.implementation,
        configurations.compileOnly,
        configurations.runtimeOnly,
        configurations.testImplementation,
        configurations.testCompileOnly,
        configurations.testRuntimeOnly,
))

fun setupResolutionStrategy(configurations: List<NamedDomainObjectProvider<Configuration>>) {
    configurations.forEach { configuration ->
        configuration {
            isTransitive = false
            resolutionStrategy {
                // dependencyUpdates tasks cannot deal with forced versions
                if (!gradle.startParameter.taskNames.contains("dependencyUpdates")) {
                    // ignore version conflicts in test dependencies, let gradle autohandle
                    // fail eagerly on version conflict (includes transitive dependencies)
                    // e.g. multiple different versions of the same dependency (group and name are equal)
                    failOnVersionConflict()

                    // cache dynamic versions for 10 minutes (these are versions like '2.4.+')
                    cacheDynamicVersionsFor(10 * 60, "seconds")
                    // don't cache changing modules at all
                    cacheChangingModulesFor(0, "seconds")
               }
            }
        }
    }
}
