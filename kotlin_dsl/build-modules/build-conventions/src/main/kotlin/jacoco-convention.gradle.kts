plugins {
    id("java-library")
    id("jacoco")
}

jacoco {
    toolVersion = ToolVersions.jacoco
}

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report

    reports {
        xml.required.set(false)
        csv.required.set(false)
    }
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
    finalizedBy(tasks.jacocoTestCoverageVerification) // report is always generated after tests run

    // ensure jacoco data is deleted before tests
    doFirst {
        delete("${buildDir}/jacoco/test.exec")
    }
}

tasks.jacocoTestCoverageVerification {
    violationRules {
        rule {
            limit {
                minimum = BigDecimal("0.00")
            }
        }

        rule {
            element = "CLASS"

            limit {
                counter = "LINE"
                value = "COVEREDRATIO"
                minimum = BigDecimal("0.70")
            }
            limit {
                counter = "BRANCH"
                value = "COVEREDRATIO"
                minimum = BigDecimal(".70")
            }
            limit {
                counter = "METHOD"
                value = "COVEREDRATIO"
                minimum = BigDecimal("0.70")
            }
        }
    }
}
