import net.ltgt.gradle.errorprone.errorprone
import net.ltgt.gradle.errorprone.CheckSeverity

plugins {
    id("java-library")
    id("net.ltgt.errorprone")
}

dependencies {
    errorprone("com.google.errorprone:error_prone_core:${ToolVersions.errorProne}")
    compileOnly("com.google.errorprone:error_prone_annotations:${ToolVersions.errorProne}")
}

tasks.withType<JavaCompile>().configureEach {
    options.errorprone {
        // disableWarningsInGeneratedCode = true
        check("MissingSummary", CheckSeverity.OFF)
        check("SameNameButDifferent", CheckSeverity.OFF)

        excludedPaths.set(".*/build/.*")
    }
}
