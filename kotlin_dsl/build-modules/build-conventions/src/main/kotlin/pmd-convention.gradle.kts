import groovy.util.Node
import groovy.xml.XmlParser


plugins {
    `java-library`
    pmd
}

var lintersDir = "${project.rootDir}/build-modules/build-conventions/linters"

abstract class PmdReportTask : DefaultTask() {
    @get:Input
    abstract val outputDir: Property<String>

    init {
        outputDir.convention("${project.buildDir}")
    }

    @TaskAction
    fun displayPmdReport() {
        println(outputDir.get())

        val pmdFile = File("${outputDir.get()}/reports/pmd/main.xml")
        if (pmdFile.exists()) {
            val rootNode = XmlParser().parseText(pmdFile.readText())
            rootNode.iterator().forEachRemaining {
                fileNode ->
                println("PMD of ${project.name}:" + (fileNode as Node).attribute("name") + ':')
                fileNode.iterator().forEachRemaining {
                    violationNodeObject ->
                    val violationNode = violationNodeObject as Node
                    println("  ${violationNode.attribute("beginline")}-${violationNode.attribute("endline")}  " + violationNode.text().trim())
                }

            }
        }
    }
}

tasks.register<PmdReportTask>("displayPmdReport")

// exclude pmd using -x pmdMain
pmd {
    toolVersion = ToolVersions.pmd
//    sourceSets = listOf(sourceSets.main)
    //source(fileTree(baseDir = "src/main/java"))
    ruleSets = listOf() // else all base rulesets included
    ruleSetFiles = files("${lintersDir}/pmdRuleSet.xml")
    isIgnoreFailures = false
}

tasks.check {
    dependsOn(tasks["displayPmdReport"])
}
tasks.pmdMain {
    mustRunAfter(tasks.test)
    finalizedBy(tasks["displayPmdReport"])
}
