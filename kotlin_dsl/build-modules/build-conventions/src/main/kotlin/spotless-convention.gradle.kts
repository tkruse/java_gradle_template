plugins {
    id("com.diffplug.spotless")
}

spotless {
    // optional: limit format enforcement to just the files changed by this feature branch
    // ratchetFrom "origin/main"

    format("misc") {
        // define the files to apply `misc` to
        target("*.groovy", "*.kts", "*.md", ".gitignore")

        // define the steps to apply to those files
        trimTrailingWhitespace()
        indentWithSpaces(4)
        endWithNewline()
    }
    java {
        // apply a specific flavor of google-java-format
        eclipse()
        // allows to use // spotless:off
        toggleOffOn()
        indentWithSpaces(4)
    }
}
