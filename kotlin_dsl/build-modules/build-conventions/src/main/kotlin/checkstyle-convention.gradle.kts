plugins {
    `java-library`
    checkstyle
}


var lintersDir = "${project.rootDir}/build-modules/build-conventions/linters"

// exclude checkstyle using -x checkstyleMain
tasks.checkstyleMain {
    mustRunAfter(tasks.test)
    configProperties = mapOf("basedir" to "${lintersDir}/checkstyle")
    isIgnoreFailures = false
    configFile = File(lintersDir, "checkstyle/checkstyle.xml")
}
tasks.checkstyleTest {
    mustRunAfter(tasks.test)
    configProperties = mapOf("basedir" to "${lintersDir}/checkstyle")
    isIgnoreFailures = false
    configFile = File(lintersDir, "checkstyle/checkstyle-test.xml")
}

checkstyle {
    toolVersion = ToolVersions.checkstyle
}
