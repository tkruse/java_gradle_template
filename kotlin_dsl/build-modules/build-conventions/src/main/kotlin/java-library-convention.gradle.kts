import org.gradle.api.tasks.testing.logging.TestExceptionFormat


plugins {
    `java-library`
    id("java-dependencies-convention")
    id("checkstyle-convention")
    id("pmd-convention")
    id("spotbugs-convention")
    id("jacoco-convention")
    id("errorprone-convention")
    id("spotless-convention")
    id("idea")
    id("eclipse")
    id("com.github.ben-manes.versions")
    id("org.inferred.processors")
    id("info.solidsoft.pitest")
}


java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.addAll(arrayOf(
        "-Xlint:all",
        "-Xlint:-processing",
        "-Xlint:-serial",
        // "-Xlint:-cast",
        // "-Xlint:deprecation",
        "-Werror"
    ))
}

tasks.check {
    mustRunAfter(tasks["clean"])
}

tasks.named<Test>("test") {
    useJUnitPlatform()
    minHeapSize = "512m"

    testLogging {
        showStandardStreams = true
        exceptionFormat = TestExceptionFormat.FULL // default is "short"

    }
}
