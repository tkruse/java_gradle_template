import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogging

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    // default dependencies
    implementation(gradleApi())

    // These dependencies are required to use plugins in conventions
    implementation("org.inferred.processors:org.inferred.processors.gradle.plugin:3.6.0")
    implementation("com.github.ben-manes:gradle-versions-plugin:0.46.0")
    implementation("com.diffplug.spotless:spotless-plugin-gradle:6.18.0")
    implementation("com.github.spotbugs.snom:spotbugs-gradle-plugin:5.0.14")
    implementation("net.ltgt.gradle:gradle-errorprone-plugin:3.0.1")
    implementation("info.solidsoft.gradle.pitest:gradle-pitest-plugin:1.7.0")
}



tasks.test {
    useJUnit()
    testLogging {
        showStandardStreams = true
        setExceptionFormat("full") // default is "short"
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}
