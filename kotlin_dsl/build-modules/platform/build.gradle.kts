plugins {
    id("java-platform")
    id("platform-convention")
}

dependencies {
    constraints {
        // which other modules to constrain
        api(project(":main"))
        api(project(":util"))

        // which versions to use for transitive dependencies
        api(Libraries.guava)

        api(Libraries.junit)
    }
}
