package com.example.main;

import com.example.util.Fibonacci;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import java.nio.charset.Charset;
import java.util.Scanner;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * Runs Fibonacci CLI
     */
    public static void main(String[] args) {
        logger.info("Enter index of Fibonacci number to compute");
        try (Scanner in = new Scanner(System.in, Charset.defaultCharset().name())) {
            final int num = in.nextInt();
            if (logger.isInfoEnabled()) {
                logger.info("Result: " + Fibonacci.compute(num));
            }
        }

        // To demonstrate Spotbugs error
        System.out.println(getNull(null).length());
    }

    @CheckForNull
    public static String getNull(@Nonnull String input) {
        return null;
    }
}
