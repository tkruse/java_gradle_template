
plugins {
    id("java-library-convention")
    id("application")
}

application {
    mainClass.set("com.example.Main")
}


tasks.run<JavaExec> {
    standardInput = System.`in`
}

dependencies {
    api(platform(project(":platform")))
    implementation(project(":util"))
}

configure<info.solidsoft.gradle.pitest.PitestPluginExtension> {
    targetClasses.set(setOf("com.example.main.*"))
}
