allprojects {
    group = "com.example"
    version = project.version // taken from gradle.properties

    repositories {
        mavenCentral()
    }
}

subprojects {
    // task to show all project dependencies
    tasks.register<DependencyReportTask>("allDeps")
}
