rootProject.name = "java_gradle_template"

// things to be reused in buildSrc and build.gradle.kts.kts.kts
includeBuild("build-modules/build-conventions")

// add subprojects without the intermediate folders
hashMapOf(
        "build-modules" to listOf("platform"),
        "commons" to listOf("util"),
        "core" to listOf("main")
)
.forEach { folder, subprojects ->
    subprojects.forEach { subproject ->
        include(subproject)
        project(":$subproject").projectDir = file("$folder/$subproject")
    }
}
