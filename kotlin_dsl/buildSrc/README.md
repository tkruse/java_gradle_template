# project buildsrc

The classes created here can be used anywhere in the root project.

Any change in this project causes a rebuild of the whole project.

Use this for complex build logic that should be written in groovy and unit tested.
