import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogging

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    // default dependencies
    implementation(gradleApi())
    implementation(localGroovy())
    testImplementation("junit:junit:4.13.2")

    testRuntimeOnly("net.ltgt.gradle:gradle-errorprone-plugin:0.0.16")
    testRuntimeOnly("info.solidsoft.gradle.pitest:gradle-pitest-plugin:1.3.0")
}


tasks.test {
    useJUnit()
    testLogging {
        showStandardStreams = true
        setExceptionFormat("full") // default is "short"
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

// choosing a flat layout
sourceSets {
    main {
        java {
            setSrcDirs(listOf("src"))
        }
        kotlin {

        }
    }
    test {
        java {
            setSrcDirs(listOf("test"))
        }
    }
}