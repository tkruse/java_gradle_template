import org.gradle.api.artifacts.ComponentMetadataContext
import org.gradle.api.artifacts.ComponentMetadataRule

abstract class AlignmentRules implements ComponentMetadataRule {
    void execute(ComponentMetadataContext ctx) {
        ctx.details.with {
            if (id.group.startsWith("com.fasterxml.jackson")) {
                // declare that Jackson modules belong to the platform defined by the Jackson BOM
                belongsTo("com.fasterxml.jackson:jackson-bom:${Versions.jackson}", false)
            }
            if (id.group.startsWith("org.slf4j")) {
                // declare that Jackson modules all belong to the Slf4J virtual platform with same version numbers
                belongsTo("org.slf4j:slf4j-virtual-platform:${id.version}")
            }
        }
    }
}
