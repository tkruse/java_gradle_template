# Gradle java project tempate

Project template for a multi-module gradle project with a lot of java code quality checks enabled.

This is a collection of personal preferences of how to use gradle.

Those preferences are preferences for professional projects working with large changing teams over multiple projects with many modules, requiring discipline to avoid deteriorating code quality.

The gradle code should be minimal in lines used, even if that causes some additional complexity to understand it.
Gradle code is commonly written as often as read (in contrast to program code), so optimizing for easy changes makes sense.

In truth, while gradle tries to look declarative, with advanced complexity a lot of gradle code becomes imperative.
While that is not bad in itself, managing imperative build code in untested `.gradle` files is bad practice.

## gradle tasks

```
   # will show errorProne errors
   gradle compileJava

   # will show jacoco coverage gaps
   gradle test

   # runs over all subprojects and displays dependencies
   gradle allDeps

   # standard task, with this project runs tests, error-prone, checkstyle, pmd, spotBugs, jacoco
   gradle check -PuseSpotbugs

   # format code
   gradle spotlessApply

   # checks online for updated versions to dependencies
   gradle dependencyUpdate

   # creates mutation coverage report (as html, must be interpreted by humans)
   gradle pitest
```

## Gradle style

* Common dependencies should be treated like the JDK, they are just there
* To compile, Each module should specify **all** uncommon dependencies
* To run, each module may rely on unspecified (transitive) dependencies
* Compile time versions, test time versions and runtime must be the same
* All dependency versions should be frozen (no dynamic versions)
* ALl dependency versions / resolutions should be declared inside the same file
* All version conflicts should be manually resolved
* Avoid duplications of anything (dependency versions, dependencies)
* extract reusable / imperative code to the buildSrc folder
  * makes code testable in theory
  * allows full IDE support with Groovy
  * avoid `apply from` construct that is not IDE-tracable
* Avoid configuring modules externally (via allprojects/subprojects), the build.gradle file should contain all relevant bits for each submodule
* Use code checkers that run easily without infrastructure (no SonarQube)
* Keep project root folder to a minimum
  * Define projects at any folder nesting level in settings.gradle
* Use declarative style in `*.gradle` files, move imperative code to buildSrc folder


## Java code style

* Break the build
  * On java compile warnings
  * On Code linters warning
  * On insufficient code coverage from unit tests
* Reuse apache commons and guava
* Use code generators for bean operations
* Use spotbugs to detect NPE potential during compile time
* Limit the scope for allowed imports
